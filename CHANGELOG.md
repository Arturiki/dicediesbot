# Changelog

All notable changes to this project will be documented in this file.


## [Unreleased]

## [v1.2] - 2020-02-15

### Added

- Changelog.

### Changed

- Update "Future Work" section.

### Removed

- Bug that answered commands sent after stop once bot started again.


## [v1.1] - 2020-02-14

### Added

- Version on code.
- Version navigation.
- YouTube link to README.
- Demo video.

### Changed

- Move images to organised folders.
- Fix links on README.
- LICENSE changed to LICENCE.


## [v1.0] - 2020-01-25

### Added

- README.
- LICENSE.
- CONTRIBUTING.
- Merge request templates.
- Issue templates.
- "print" as detailed logging information. 


## [v0.1] - 2020-01-12

### Added

- Create functioning bot.
- PEP8 formatting.