# Contributing to DiceDieBot

Since the main interest is to learn and help the commnity, you're free to:

a) open issues, and
b) send PRs

to whatever is going to improve this development.

Each PR will be reviewed by the contributors and, if accepted, it'll be merged to the master branch. After this, you'll now be considered as a contributor and your name can be added to the contributors list in the
[README](README.md) file.
