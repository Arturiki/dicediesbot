# DiceDieBot v1.2: Dice Roll Simulator


## About DiceDieBot

DiceDieBot is a great contribution for any rol games occurring in Telegram
(eg. DnD games) or anybody that wants to simulate dice rolls of different
amount of dices and faces.
 
DiceDieBot includes a custom keyboard with the most popular rolls and allows
to roll additional custom rolls using a command.
 
You can find DiceDieBot at:
* YouTube: https://youtu.be/j8vJBvbO_LM
* Telegram: [https://t.me/DiceDiesBot](https://t.me/DiceDiesBot)

------
## How to use DiceDieBot

This section displays all possible commands and interactions with DiceDieBot.

***/start***  
Starts the bot, displays the custom keyboard and some general information.
Pressing a button of the custom keyboard will answer with the result of the
roll.

*Hint: Tossing a coin will get a result of "Heads" or "Tails" instead of
numbers.* 

![/start command](media/readme/start.png)

***/help***  
Displays a detailed message with all relevant information.

***/roll [customRoll]***  

Gets the result of the specified custom roll following the format *XdY*,
where *X* is the number of dices and and *Y* is the number of faces of the
dices. *X* > 1, so there is always at least one die; *Y* > 2 so there is at
least more than one possible result.

To roll a custom example of 3 dices with 6 faces: */roll 3d6*.
If the optional parameter *customRoll* is empty, then the default roll will
 be 2 dices with 6 faces (i.e. *2d6*).

*Hint: To toss one or more coins, use the format* Xd2 *(e.g.* 2d2 *for 2
 coins).* 

![/roll command](media/readme/roll.png)

***/stop***  
Stops the bot and hides the custom keyboard.

![/stop command](media/readme/stop.png)

***/donate***  
Displays a menu to choose a donation amount via PayPal. Very kindly appreciated.

***Using the custom keyboard***  
By clicking on a button of the keyboard, the bot will return the result of the
chosen roll. 

*Hint: If the roll gets the best result (e.g. double 6s on* 2d6 *roll) or the
worst result (e.g. double 1s on* 2d6 *roll), a random witty comment will be
 added
 to the displayed message.* 

![witty comment](media/readme/witty.png)


------

## Future work

* Add an option to enable/disable the witty comments when rolling the best and
 worst results (e.g. double 6s in a 2d6).
* Allow the user to change the default roll when using */roll* without a
 parameter.
* Complete documentation.

## Changelog

Please check our [CHANGELOG](CHANGELOG.md)
file.


## Contribution

Please check our [CONTRIBUTING](CONTRIBUTING.md)
file.

#### Contributor list:
* [Arturo Díaz Coca](https://gitlab.com/Arturiki)

------

## Licence

Please read the [LICENCE](LICENCE.md)
provided in this repo.

------

## Donating

All donations received from DiceDieBot will be used for the server mantainance
and the bot development.

Thank you very much for considering this option. 